package com.nineyi.android.androiddailybuild

import android.app.NotificationChannel
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.IBinder
import android.support.v4.app.NotificationCompat
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.Request
import okio.Okio
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import java.io.File


/**
 * Created by shaocheng on 2017/11/22.
 */
class DailyBuildService : Service() {
    override fun onBind(p0: Intent?): IBinder {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        val channelId = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            NotificationChannel.DEFAULT_CHANNEL_ID else "miscellaneous"
        val notification = NotificationCompat.Builder(this, channelId)
                .setContentTitle("正在下載最新版本...")
                .setContentText("請稍候")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setOngoing(true)
        startForeground(777, notification.build())

        val file = File(externalCacheDir.absoluteFile, "latest.apk")

        Single.fromCallable {
            val client = OkHttpClient()
            val request = Request.Builder().url(url).build()
            val response = client.newCall(request).execute()

            val sink = Okio.buffer(Okio.sink(file))
            response.body()?.source()?.let {
                sink.writeAll(it)
                sink.close()
            }

            response.header("last-modified")
        }
        .subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe { res, _ ->

            var date: String = res.toString()
            try {
                val formatter = DateTimeFormat.forPattern("EEE, dd MMM yyyy HH:mm:ss zzz").withZoneUTC()
                date = formatter.parseDateTime(date).withZone(DateTimeZone.getDefault()).toString()
            } catch (e: Exception) { }

            val i = Intent(Intent.ACTION_VIEW)
            i.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive")
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            val pIntent = PendingIntent.getActivity(this, 0, i, 0)

            val noti = NotificationCompat.Builder(this, channelId)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("已下載好了最新版本 $date")
                    .setContentText("點擊即可安裝")
                    .setContentIntent(pIntent)
                    .setOngoing(true)
            startForeground(777, noti.build())

        }
        return Service.START_STICKY
    }
}