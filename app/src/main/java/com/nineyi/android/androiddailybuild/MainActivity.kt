package com.nineyi.android.androiddailybuild

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.Request
import okio.Okio
import org.joda.time.DateTime
import java.io.File


val url = "https://appgen.91app.com/build/QA4/Shop/s000052/android/LATEST_JOB/s000052_v2.24.0_Qa4_LATEST.apk"

class MainActivity : AppCompatActivity() {

    val selfUpdateUrl = "https://bitbucket.org/shaocheng91/androiddailybuild/raw/master/release/LATEST.apk"

    lateinit var file: File
    lateinit var selfFile: File

    lateinit var installButton: Button
    lateinit var updateSelfButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        file = File(externalCacheDir.absoluteFile, "latest.apk")
        selfFile = File(externalCacheDir.absoluteFile, "self_latest.apk")

        setContentView(R.layout.activity_main)

        installButton = findViewById<Button>(R.id.install_latest)
        installButton.setOnClickListener {
            val service = Intent(this, DailyBuildService::class.java)
            startService(service)
        }

        updateSelfButton = findViewById<Button>(R.id.update_self)
        updateSelfButton.setOnClickListener {
            updateSelfButton.isEnabled = false

            Completable.fromCallable {
                val client = OkHttpClient()
                val request = Request.Builder().url(selfUpdateUrl).build()
                val response = client.newCall(request).execute()

                val sink = Okio.buffer(Okio.sink(selfFile))
                response.body()?.source()?.let {
                    sink.writeAll(it)
                    sink.close()
                }
            }
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                updateSelfButton.isEnabled = true

                val intent = Intent(Intent.ACTION_VIEW)
                intent.setDataAndType(Uri.fromFile(selfFile), "application/vnd.android.package-archive")
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
            }
        }

        val intent = Intent(this, AlarmReceiver::class.java)
        val alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0)

        val dt = DateTime()
                .withHourOfDay(10)
                .withMinuteOfHour(0)
                .withSecondOfMinute(0)
        val alarmMgr = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, dt.millis,
                AlarmManager.INTERVAL_DAY, alarmIntent)
    }
}
