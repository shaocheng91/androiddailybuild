package com.nineyi.android.androiddailybuild

import android.content.Intent
import android.content.BroadcastReceiver
import android.content.Context
import android.widget.Toast
import android.os.PowerManager




/**
 * Created by shaocheng on 2017/11/22.
 */
class AlarmReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val pm = context.getSystemService(Context.POWER_SERVICE) as PowerManager
        val wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "")
        wl.acquire()

        val service = Intent(context, DailyBuildService::class.java)
        context.startService(service)


        wl.release()
    }
}